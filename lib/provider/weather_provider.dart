import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:weather/models/my_location.dart';
import 'package:weather/models/weather.dart';
import 'package:weather/models/weatherbrain.dart';
import 'package:weather/repository/repository.dart';

class WeatherProvider with ChangeNotifier {
  bool _isLoading = true;
  MyWeather? _myWeather;
  WeatherModel? _weatherModel = WeatherModel();

  void getLocation() async {
    MyLocation location = MyLocation();
    await location.getLocation();
    double? lat = location.latitude;
    double? lon = location.longitude;
    loadWeather(lat!, lon!);
  }
  Future<void> loadWeatherByCityName(String cityName) async {
    try {
      Response response =
      await WeatherRepository().loadWeatherByCityName(cityName);
      if (response.statusCode == 200) {
        _myWeather = MyWeather.fromJson(response.data);
        isLoading = false;
        notifyListeners();
      } else {
        isLoading = false;
        notifyListeners();

      }
    } catch (e) {
      print(e);
      isLoading = false;
      notifyListeners();
    }
  }
  Future<void> loadWeather(double lat, double lon) async {
    try {
      Response response = await WeatherRepository().loadWeather(lat, lon);
      if (response.statusCode == 200) {
        _myWeather = MyWeather.fromJson(response.data);
        isLoading = false;
        notifyListeners();
      } else {
        isLoading = false;
        notifyListeners();
      }
    } catch (e) {
      isLoading = false;
      notifyListeners();
      print(e);
    }
  }

  MyWeather get myWeather => _myWeather!;

  bool get isLoading => _isLoading;

  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  WeatherModel get weatherModel => _weatherModel!;

  set weatherModel(WeatherModel value) {
    _weatherModel = value;
    notifyListeners();
  }


}
