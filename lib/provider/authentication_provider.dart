import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:weather/models/LoginResponse.dart';
import 'package:weather/models/my_location.dart';
import 'package:weather/models/weather.dart';
import 'package:weather/models/weatherbrain.dart';
import 'package:weather/repository/authentication_repository.dart';
import 'package:weather/repository/repository.dart';

class AuthenticationProvider with ChangeNotifier {
  bool? _isLoading;
  LoginResponse? _myResponse;

  void loginApiHit(String email, String password) async {
    _isLoading = true;
    notifyListeners();
    Response response =
        await AuthenticationRepository().loginApiCall(email, password);

    if (response.statusCode == 200) {
      _myResponse = LoginResponse.fromJson(response.data);
      print(_myResponse!.user.name);
      print(_myResponse!.user.email);
    }
    _isLoading = false;
    notifyListeners();
  }

  LoginResponse get myResponse => _myResponse!;

  bool get isLoading => _isLoading!;

  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }
}
