import 'package:dio/dio.dart';
import 'package:weather/utilities/constants.dart';


class AuthenticationRepository {

  Future<Response> loginApiCall(String email, String password) async {
    Response? loginResponse;

    loginResponse = await Dio().post(LOGIN_API, data: <String, String>{
      "email": email,
      "password": password,
    }).timeout(timeoutDuration);

    return loginResponse;
  }
}

