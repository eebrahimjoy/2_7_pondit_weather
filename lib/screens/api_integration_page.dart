import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:weather/provider/authentication_provider.dart';

class ApiIntegrationPage extends StatefulWidget {
  const ApiIntegrationPage({Key? key}) : super(key: key);

  @override
  _ApiIntegrationPageState createState() => _ApiIntegrationPageState();
}

class _ApiIntegrationPageState extends State<ApiIntegrationPage> {
  TextEditingController? _emailController;

  TextEditingController? _passwordController;

  @override
  void initState() {
    super.initState();
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
    _emailController!.text = "eebrahimjoy@gmail.com";
    _passwordController!.text = "87654321";
    Provider.of<AuthenticationProvider>(context, listen: false).isLoading =
        false;
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<AuthenticationProvider>(
      builder: (_, provider, __) {
        return Scaffold(
          body: provider.isLoading == false
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Flexible(
                      child: TextField(
                        controller: _emailController,
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Email',
                        ),
                      ),
                    ),
                    Flexible(
                      child: TextField(
                        controller: _passwordController,
                        obscureText: true,
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Password',
                        ),
                      ),
                    ),
                    TextButton(
                        onPressed: () {
                          provider.loginApiHit(_emailController!.text,
                              _passwordController!.text);
                        },
                        child: Text("HIT API"))

                  ],
                )
              : Center(child: const LinearProgressIndicator()),
        );
      },
    );
  }
}
