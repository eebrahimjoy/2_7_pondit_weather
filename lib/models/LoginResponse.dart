class LoginResponse {
  bool? _success;
  String? _accessToken;
  User? _user;


  bool get success => _success!;

  LoginResponse.fromJson(Map<String, dynamic> json) {

    _success = json['success'];
    _accessToken = json['access_token'];
    _user = User.fromJson(json['user']);
  }

  String get accessToken => _accessToken!;

  User get user => _user!;
}

class User {
  String? _name;
  String? _email;


  String get name => _name!;
  String get email => _email!;

  User.fromJson(Map<String, dynamic> json) {

    _name = json['name'];
    _email = json['email'];
  }


}
