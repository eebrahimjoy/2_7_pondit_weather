import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:weather/provider/authentication_provider.dart';
import 'package:weather/provider/weather_provider.dart';
import 'package:weather/screens/api_integration_page.dart';

void main() => runApp(
      MultiProvider(
        providers: [
          ChangeNotifierProvider<WeatherProvider>(
              create: (context) => WeatherProvider()),
          ChangeNotifierProvider<AuthenticationProvider>(
              create: (context) => AuthenticationProvider()),
        ],
        child: MyApp(),
      ),
    );

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark(),
      home: ApiIntegrationPage(),
    );
  }
}
